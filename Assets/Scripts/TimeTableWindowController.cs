﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTableWindowController 
{
	private TimeTableWindow _visual;
	private readonly BookingWindowController _booking;
	private readonly Trip _trip;

	public TimeTableWindowController(BookingWindowController booking, Trip trip)
	{
		_trip = trip;
		_booking = booking;
		_booking.Back += OnBookingBack;

		ShowWindow ();
	}

	public void ShowWindow()
	{
		_visual = GameObject.Instantiate(Resources.Load<TimeTableWindow>("TimeTableWindow"));

		List<string> cities = new List<string> () { "Воронеж", "Донецк", "Крым", "Курск", "Москва", "Платов", "Ростов", "Питер", "Тула", "Феодосия", "Ялта"};
		_visual.SetCitiesList (cities, _trip.GetDispatch(), _trip.GetArrive());
		_visual.Continue += OnContinue;
	}

	void OnContinue()
	{
		string message = "";
		var ok = _trip.Create (_visual.GetDispatchCity (), _visual.GetArriveCity (), _visual.GetDate (), ref message);
		if (ok) {
			_visual.Continue -= OnContinue;
			GameObject.Destroy (_visual.gameObject);
			_booking.ShowWindow ();
		} 
		else 
		{
			_visual.SetMessage (message);
		}
	}

	void OnBookingBack()
	{
		ShowWindow ();
	}
}
