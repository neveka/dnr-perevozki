﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeTableWindow : MonoBehaviour 
{
	public event System.Action Continue;
	public Dropdown _dispatch;
	public Dropdown _arrive;
	public Button _continueButton;
	public DatePicker _datePicker;
	public Text _messageText;

	public void SetCitiesList(List<string> cities, string dispatchSelection, string arriveSelection)
	{
		List<Dropdown.OptionData> options = cities.ConvertAll (c => new Dropdown.OptionData (c));
		_dispatch.options = options;
		_dispatch.value = cities.IndexOf (dispatchSelection);
		_arrive.options = options;
		_arrive.value = cities.IndexOf (arriveSelection);
		_continueButton.onClick.RemoveAllListeners();
		_continueButton.onClick.AddListener (() => {
			Continue();
		});	
	}

	public string GetDispatchCity()
	{
		return _dispatch.options[_dispatch.value].text;
	}

	public string GetArriveCity()
	{
		return _arrive.options[_arrive.value].text;
	}

	public string GetDate()
	{
		return _datePicker.SelectedDateText.text;
	}

	public void SetMessage(string text)
	{
		_messageText.text = text;
	}
}
