﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppInstaller : MonoBehaviour 
{
	void Awake()
	{
		var trip = new Trip (this);
		var booking = new BookingWindowController (trip);
		var timeTable = new TimeTableWindowController (booking, trip);
	}
}
