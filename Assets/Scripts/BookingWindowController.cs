﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookingWindowController 
{
	public event System.Action Back;
	private BookingWindow _visual;
	private readonly Trip _trip;

	public BookingWindowController(Trip trip)
	{
		_trip = trip;
	}

	public void ShowWindow()
	{
		_visual = GameObject.Instantiate(Resources.Load<BookingWindow>("BookingWindow"));
		_visual.SetTrip (_trip.GetInfo(), _trip.GetName(), _trip.GetPhone());
		_visual.SelectTrip += OnSelectTrip;
		_visual.MakeOrder += OnMakeOrder;
		_trip.OrderSentToServerResult += OnOrderSentToServerResult;
	}

	void OnSelectTrip()
	{
		string message = "";
		_trip.Remember (_visual.GetName (), _visual.GetPhone (), ref message);
		_visual.SelectTrip -= OnSelectTrip;
		_visual.MakeOrder -= OnMakeOrder;
		_trip.OrderSentToServerResult -= OnOrderSentToServerResult;
		GameObject.Destroy (_visual.gameObject);
		Back ();
	}

	void OnMakeOrder()
	{
		string message = "";
		bool ok = _trip.MakeOrder (_visual.GetName (), _visual.GetPhone (), _visual.GetNumber(), ref message);
		_visual.SetMessage (message, ok);
	}

	void OnOrderSentToServerResult(string message, bool ok)
	{
		_visual.SetMessage (message, ok);
	}
}
