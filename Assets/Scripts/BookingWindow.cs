﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookingWindow : MonoBehaviour 
{
	public event System.Action SelectTrip;
	public event System.Action MakeOrder;
	public Button _selectTripButton;
	public Button _orderButton;
	public Text _tripInfo;
	public Text _messageText;
	public Text _nameText;
	public Text _numberText;
	public Text _phoneText;
	public Image _okImage;
	public Image _failImage;

	public void SetTrip(string tripInfo, string name, string phone)
	{
		_tripInfo.text = tripInfo;
		_selectTripButton.onClick.RemoveAllListeners();
		_selectTripButton.onClick.AddListener (() => {
			SelectTrip();
		});	

		_orderButton.onClick.RemoveAllListeners();
		_orderButton.onClick.AddListener (() => {
			MakeOrder();
		});	

		if (!string.IsNullOrEmpty (name))
			_nameText.gameObject.GetComponentInParent<InputField>().text = name;

		if(!string.IsNullOrEmpty (phone))
			_phoneText.gameObject.GetComponentInParent<InputField>().text = phone;
	}

	public string GetName()
	{
		return _nameText.text;
	}

	public string GetNumber()
	{
		return _numberText.text;
	}

	public string GetPhone()
	{
		return _phoneText.text;
	}

	public void SetMessage(string text, bool ok)
	{
		_messageText.text = text;
		_failImage.gameObject.SetActive (!ok);
		_okImage.gameObject.SetActive (ok);
	}
}
