﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Trip 
{
	public event System.Action<string, bool> OrderSentToServerResult;

	private string _info;
	private string _name;
	private string _phone;
	private string _numberString;
	private string _dispatch;
	private string _arrive; 
	private string _date;

	private readonly MonoBehaviour _coroutineStarter;

	public Trip(MonoBehaviour coroutineStarter)
	{
		_coroutineStarter = coroutineStarter;
	}

	public string GetInfo()
	{
		return _info;
	}

	public string GetName()
	{
		return _name;
	}

	public string GetPhone()
	{
		return _phone;
	}

	public string GetArrive()
	{
		return string.IsNullOrEmpty(_arrive)?"Донецк":_arrive;
	}

	public string GetDispatch()
	{
		return string.IsNullOrEmpty(_dispatch)?"Москва":_dispatch;
	}
		
	public bool Create(string dispatch, string arrive, string date, ref string message)
	{
		if (dispatch.Equals (arrive)) 
		{
			message = "Выберите разные пункты назначения";
			return false;
		}
		_arrive = arrive;
		_dispatch = dispatch;
		_date = date;
		_info = dispatch+"-"+arrive+" "+date;
		return true;
	}

	public bool Remember(string name, string phone, ref string message)
	{
		message = "";
		if (string.IsNullOrEmpty (name)) 
		{
			message = "Введите ФИО";
			return false;
		}
		_name = name;

		if (string.IsNullOrEmpty (phone)) 
		{
			message = "Введите телефон";
			return false;
		}
		if (phone.Length != 10) 
		{
			message = "Введите 10 цифр телефона без кода страны";
			return false;
		}
		_phone = phone;
		return true;
	}

	public bool MakeOrder(string name, string phone, string numberString, ref string message)
	{
		if (!Remember (name, phone, ref message))
			return false;
		int number = 1;
		if (int.TryParse (numberString, out number)) 
		{
			if (number < 1) 
			{
				message = "Введите число мест не меньше одного";
				return false;
			}

			if (number > 8) 
			{
				message = "Введите число мест меньше 8-ми";
				return false;
			}
		} 
		else 
		{
			number = 1;
		}
		_numberString = number.ToString();
	
		message = "Отправляем заявку...";
		_coroutineStarter.StartCoroutine (SendOrderToServer());
		return true;
	}

	IEnumerator SendOrderToServer()
	{
		var wwwForm = new WWWForm ();
		wwwForm.AddField ("submit", "Забронировать поездку");
		wwwForm.AddField ("name", _name);
		wwwForm.AddField ("phone", _phone);
		wwwForm.AddField ("number", _numberString);
		wwwForm.AddField ("info", _info);

		UnityWebRequest www = UnityWebRequest.Post("https://www.dnr-perevozki.ru/booking.php", wwwForm);
		yield return www.Send();

		if(www.isNetworkError || www.isHttpError) {
			Debug.LogError (www.error);
			OrderSentToServerResult("Ошибка при отправке заявки, повторите позже.", false);
		}
		else {
			OrderSentToServerResult("Заявка успешно отправлена. Вам перезвонят.", true);
		}
	}
}
